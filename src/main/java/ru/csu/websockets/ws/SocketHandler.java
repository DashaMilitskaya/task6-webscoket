package ru.csu.websockets.ws;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import ru.csu.websockets.service.SubscriptionService;

@Component
@RequiredArgsConstructor
public class SocketHandler extends TextWebSocketHandler {

    private final SubscriptionService subscriptionService;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        subscriptionService.subscribe(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        subscriptionService.handleMessage(session, message);
    }

    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        super.handlePongMessage(session, message);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session, exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        subscriptionService.deSubscribe(session);

    }
}
